# aststructgen
## Description
trying to create a structure and a variable using that structure
my feelings:
* lots of overhead
* variable using structure does not format with newline for array items

you can watch the generated code here: [mystruct.go.bak](mystruct.go.bak)
