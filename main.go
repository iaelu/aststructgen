package main

import (
	"fmt"
	"go/ast"
	"go/printer"
	"go/token"
	"os"
)
func main() {

	f := newAstFile("mylib")

	struct1, structobj1 := newAstStruct("MyStruct")
	addTypeStructToFile(f, struct1, structobj1)

	field1, unres1 := newAstField("Field1", "string")
	addFieldToStruct(struct1, field1)
	addUnresolvedToFile(f, unres1)
	field2, unres2 := newAstField("Field2", "string")
	addFieldToStruct(struct1, field2)
	addUnresolvedToFile(f, unres2)
	field3, unres3 := newAstField("Field3", "string")
	addFieldToStruct(struct1, field3)
	addUnresolvedToFile(f, unres3)

	array1, arrayobj1 := newArrayVar("TypeFields", structobj1)
	addVarToFile(f, array1, arrayobj1)

	elem1 := newElement()
	addKVtoElem(elem1, "Field1", "toto", token.STRING)
	addKVtoElem(elem1, "Field2", "titi", token.STRING)
	addKVtoElem(elem1, "Field3", "tata", token.STRING)
	addElemToArray(array1, elem1)
	elem2 := newElement()
	addKVtoElem(elem2, "Field1", "toto", token.STRING)
	addKVtoElem(elem2, "Field2", "titi", token.STRING)
	addKVtoElem(elem2, "Field3", "tata", token.STRING)
	addElemToArray(array1, elem2)
	elem3 := newElement()
	addKVtoElem(elem3, "Field1", "toto", token.STRING)
	addKVtoElem(elem3, "Field2", "titi", token.STRING)
	addKVtoElem(elem3, "Field3", "tata", token.STRING)
	addElemToArray(array1, elem3)

	printer.Fprint(os.Stdout, token.NewFileSet(), f)
}

func newAstFile(pkg string) *ast.File {
	return &ast.File{
		Name:    ast.NewIdent(pkg),
		Package: token.Pos(1),
	}
}

func addTypeStructToFile(f *ast.File, spec *ast.TypeSpec, scopeObj *ast.Object) {
	decl := &ast.GenDecl{
		Tok:   token.TYPE,
		Specs: []ast.Spec{spec},
	}
	if f.Decls == nil {
		f.Decls = make([]ast.Decl, 0)
	}
	f.Decls = append(f.Decls, decl)
	if f.Scope == nil {
		f.Scope = ast.NewScope(nil)
	}
	f.Scope.Insert(scopeObj)
}

func addVarToFile(f *ast.File, spec *ast.ValueSpec, scopeObj *ast.Object) {
	decl := &ast.GenDecl{
		Tok:   token.VAR,
		Specs: []ast.Spec{spec},
	}
	if f.Decls == nil {
		f.Decls = make([]ast.Decl, 0)
	}
	f.Decls = append(f.Decls, decl)
	if f.Scope == nil {
		f.Scope = ast.NewScope(nil)
	}
	f.Scope.Insert(scopeObj)
}

func newAstStruct(name string) (spec *ast.TypeSpec, scopeObj *ast.Object) {
	spec = &ast.TypeSpec{
		Name: ast.NewIdent(name),
		Type: &ast.StructType{
			Fields: &ast.FieldList{
				List: make([]*ast.Field, 0),
			},
		},
	}
	scopeObj = &ast.Object{
		Kind: ast.Typ,
		Name: name,
		Decl: spec,
	}
	spec.Name.Obj = scopeObj
	return
}

func addFieldToStruct(spec *ast.TypeSpec, field *ast.Field) {
	st := spec.Type.(*ast.StructType)
	st.Fields.List = append(st.Fields.List, field)
}

func addUnresolvedToFile(f *ast.File, unres *ast.Ident) {
	if f.Unresolved == nil {
		f.Unresolved = make([]*ast.Ident, 0)
	}
	f.Unresolved = append(f.Unresolved, unres)
}

func newAstField(name, typ string) (field *ast.Field, unres *ast.Ident) {
	unres = ast.NewIdent(typ)
	field = &ast.Field{
		Names: []*ast.Ident{
			ast.NewIdent(name),
		},
		Type: unres,
	}
	fieldObj := &ast.Object{
		Kind: ast.Var,
		Name: name,
		Decl: field,
	}
	field.Names[0].Obj = fieldObj
	return
}

func newArrayVar(name string, typeObj *ast.Object) (spec *ast.ValueSpec, scopeObj *ast.Object) {
	spec = &ast.ValueSpec{
		Names:  make([]*ast.Ident, 0),
		Values: make([]ast.Expr, 0),
	}
	scopeObj = &ast.Object{
		Kind: ast.Var,
		Name: name,
		Decl: spec,
		Data: 0,
	}
	valueName := ast.NewIdent(name)
	valueName.Obj = scopeObj
	spec.Names = append(spec.Names, valueName)
	values := &ast.CompositeLit{
		Type: &ast.ArrayType{
			Elt: ast.NewIdent(typeObj.Name),
		},
		Elts: make([]ast.Expr, 0),
	}
	values.Type.(*ast.ArrayType).Elt.(*ast.Ident).Obj = typeObj
	spec.Values = append(spec.Values, values)
	return
}

func newElement() *ast.CompositeLit {
	return &ast.CompositeLit{
		Elts: make([]ast.Expr, 0),
	}
}

func addKVtoElem(elem *ast.CompositeLit, key, value string, kind token.Token) {
	kv := &ast.KeyValueExpr{
		Key: ast.NewIdent(key),
		Value: &ast.BasicLit{
			Kind:  kind,
			Value: fmt.Sprintf("\"%s\"", value),
		},
	}
	elem.Elts = append(elem.Elts, kv)
}

func addElemToArray(spec *ast.ValueSpec, elem *ast.CompositeLit) {
	values := spec.Values[0].(*ast.CompositeLit)
	values.Lbrace = token.Pos(1)
	pos := token.Pos(len(values.Elts) + 1)
	elem.Lbrace = values.Lbrace + pos
	elem.Rbrace = values.Lbrace + pos
	values.Elts = append(values.Elts, elem)
	values.Rbrace = values.Lbrace + pos + 1
}
